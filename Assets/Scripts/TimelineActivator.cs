﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Playables;

[RequireComponent(typeof(Collider))]
public class TimelineActivator : MonoBehaviour
{

    public PlayableDirector playableDirector;     //Reference to the PlayableDiretor component that contains the Timeline
    public string playerTAG;                      //Tag to check against when some object enter or exit the trigger
    public Transform interactionLocation;         //Reference to the transform where the player will be placed when the cinematic starts
    public bool autoActivate = false;             //Will be the cinematic activated when an object with a valid TAG enters the trigger.
    
    
    public bool interact { get; set; }            //Property activated from outside this object that behaves like an input button.

    [Header("Activation Zone events")]
    public UnityEvent OnPlayerEnter;              //Events to raise when an object with valid Tag enters the trigger
    public UnityEvent OnPlayerExit;               //Events to raise when an object with valid Tag exits the trigger

    [Header("Timeline Events")]
    public UnityEvent OnTimeLineStart;            //Events to raise when the Timeline starts playing.
    public UnityEvent OnTimeLineEnd;              //Events to raise when the Timeline stops playing.

    private bool isPlaying;                       //Determines if the Timeline is currently playing
    private bool playerInside;                    //Determines if the player is inside of the trigger or not
    private Transform playerTransform;            //Reference to the player Transform

    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag.Equals(playerTAG))
        {
            playerInside = true;
            playerTransform = other.transform;
            OnPlayerEnter.Invoke();
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag.Equals(playerTAG))
        {
            playerInside = false;
            playerTransform = null;
            OnPlayerExit.Invoke();
        }
    }

    private void PlayTimeline()
    {
        //Place the character at the correct interaction position
        if (playerTransform && interactionLocation)
            playerTransform.SetPositionAndRotation(interactionLocation.position, interactionLocation.rotation);

        //Avoid infinite interaction loop
        if (autoActivate)
            playerInside = false;

        //Play the Timeline
        if (playableDirector)
            playableDirector.Play();

        //Set variables
        isPlaying = true;
        interact = false;

        //Wait for Timeline to end
        StartCoroutine(WaitForTimeLineToEnd());
    }

    private IEnumerator WaitForTimeLineToEnd()
    {
        //Invoke the methods linked to
        //the beginning of the cinematic
        OnTimeLineStart.Invoke();

        //Get the duration of the timeline from the playable Director
        float timeLineDuration = (float)playableDirector.duration;

        //Wait until the cinematic playing is over
        while(timeLineDuration > 0)
        {
            timeLineDuration -= Time.deltaTime;
            yield return null;
        }

        //Reset variable
        isPlaying =  false;

        //Invoke the methods liked to
        //the end of the cinematic
        OnTimeLineEnd.Invoke();
    }
    // Update is called once per frame
    private void Update()
    {
        if(playerInside && !isPlaying)
        {
            if(interact || autoActivate)
            {
                PlayTimeline();
            }
        }
    }
}
